import schedulers.*;
import task.Task;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * Test different scheduling algorithms qualities from samples
     */
    public static void main(String[] args) throws IOException {
        List<Scheduler> schedulers = new ArrayList<>();

        schedulers.add(new RandomScheduler());
        schedulers.add(new LatenessConstructiveScheduler());
        schedulers.add(new LengthTimesWeightConstructiveScheduler());
        schedulers.add(new WeightConstructiveScheduler());
        schedulers.add(new SimpleLocalSearchScheduler());

        for (String pathname : args) {
            System.out.println("Scheduling tasks from name: " + pathname);
            System.out.println();

            List<Task> tasks = getTasksFromFile(pathname);

            for (Scheduler scheduler : schedulers) {
                // Make a copy of the initial tasks for the same configuration
                System.out.println("Scheduler: " + scheduler.getClass().getSimpleName());

                List<Integer> indices = scheduler.scheduleTasks(tasks);
                System.out.println("Quality: " + computeSchedulerQuality(tasks, indices));

                System.out.println();
                for (Integer index : indices) {
                    System.out.println(index);
                }
            }
        }
    }

    /**
     * Compute the scheduler quality, this is done by making the sum of the tasks which exceeded the time limit
     * times their weight
     *
     * @param tasks The input tasks
     * @param indices The scheduled tasks indices
     * @return A quality, lower is better
     */
    public static int computeSchedulerQuality(List<Task> tasks, List<Integer> indices) {
        int quality = 0;
        int currentTime = 0;

        for (int i = 0; i < tasks.size(); i++) {
            Task task = tasks.get(indices.get(i));
            currentTime += task.getLength();
            // Only add to quality if the current time exceeded the time limit
            quality += Math.max(currentTime - task.getLimitTime(), 0) * task.getWeight();
        }
        return quality;
    }

    /**
     * Retrieve a list of tasks from a file,
     * The file should have the following structure
     * [LENGTH] [WEIGHT] [TIME LIMIT]
     *
     * @param pathname The absolute path of the tasks file
     * @return The list of tasks of the file
     * @throws IOException When the specified file was not found
     */
    public static List<Task> getTasksFromFile(String pathname) throws IOException {
        List<Task> tasks = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(pathname));
        String line = bufferedReader.readLine();
        int lineCount = Integer.parseInt(line);
        for (int i = 0; i < lineCount; i++) {
            line = bufferedReader.readLine();
            String[] lineData = line.split(" ");
            tasks.add(new Task(
                    Integer.parseInt(lineData[0]),
                    Integer.parseInt(lineData[1]),
                    Integer.parseInt(lineData[2])
            ));
        }
        return tasks;
    }
}
