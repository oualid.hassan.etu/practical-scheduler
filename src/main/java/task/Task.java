package task;

public class Task {
    private final int length;
    private final int weight;
    private final int limitTime;

    /**
     * Creates a new Task
     * @param startTime The start time of the task
     * @param weight The weight of the task
     * @param limitTime The time limit of the task, should not be lesser than start time
     */
    public Task(int startTime, int weight, int limitTime) {
        this.length = startTime;
        this.weight = weight;
        this.limitTime = limitTime;
    }

    /**
     * Return the quality of this task with the specified time
     * @param time The time used to compute the quality of the task
     * @return The task quality at the specified time
     */
    public int getQualityAtTime(int time) {
        return Math.max(time - limitTime, 0) * weight;
    }

    public int getLength() {
        return length;
    }

    public int getWeight() {
        return weight;
    }

    public int getLimitTime() {
        return limitTime;
    }

    @Override
    public String toString() {
        return "task.Task{" +
                "length=" + length +
                ", weight=" + weight +
                ", limitTime=" + limitTime +
                '}';
    }
}
