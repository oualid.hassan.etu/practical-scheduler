package schedulers;

import task.Task;

import java.util.ArrayList;
import java.util.List;

public class WeightConstructiveScheduler implements Scheduler {

    /**
     * A constructive scheduler using the weight of a task
     */
    public WeightConstructiveScheduler() {
        super();
    }

    @Override
    public List<Integer> scheduleTasks(List<Task> tasks) {
        ArrayList<Integer> indices = new ArrayList<>();
        for (int i = 0; i < tasks.size(); i++) {
            Task bestTask = getTaskFromWeight(tasks, indices);
            indices.add(tasks.indexOf(bestTask));
        }
        return indices;
    }

    /**
     * Get the best task depending on the weight of the task
     *
     * @param tasks   The list of tasks
     * @param indices The list of indices of tasks to exclude
     * @return The best task found
     */
    private Task getTaskFromWeight(List<Task> tasks, List<Integer> indices) {
        return tasks.stream()
                .filter(task -> !indices.contains(tasks.indexOf(task)))
                .min((o1, o2) -> o2.getWeight() - o1.getWeight())
                .orElseThrow();
    }
}
