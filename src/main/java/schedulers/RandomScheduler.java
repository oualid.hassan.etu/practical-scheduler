package schedulers;

import task.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomScheduler implements Scheduler {

    /**
     * Random scheduler using the {@link Collections) shuffle method};
     */
    public RandomScheduler() {
        super();
    }

    @Override
    public List<Integer> scheduleTasks(List<Task> tasks) {
        ArrayList<Integer> indices = new ArrayList<>();
        for (int i = 0; i < tasks.size(); i++) {
            indices.add(i);
        }
        Collections.shuffle(indices);
        return indices;
    }
}
