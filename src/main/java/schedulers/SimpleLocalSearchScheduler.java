package schedulers;

import task.Task;

import java.util.List;

public class SimpleLocalSearchScheduler implements Scheduler {

    public SimpleLocalSearchScheduler() {
        super();
    }

    @Override
    public List<Integer> scheduleTasks(List<Task> tasks) {
        RandomScheduler randomScheduler = new RandomScheduler();
        List<Integer> configuration = randomScheduler.scheduleTasks(tasks);

        for (int i = 0; i < tasks.size(); i++) {
            Task currentTask = tasks.get(i);
            int currentTime = getTimeAtIndex(tasks, configuration, i);
            List<Task> lateTasks = tasks.stream()
                    .filter(task -> task.getQualityAtTime(currentTime) > 0)
                    .toList();
            int currentQuality = getQuality(tasks, configuration);

            // Find all late tasks
            for (Task lateTask : lateTasks) {
                // Swap the current task with the late task
                this.swapTasks(tasks, configuration, currentTask, lateTask);

                // Check the result configuration quality
                if (getQuality(tasks, configuration) < currentQuality) {
                    // The result configuration is better
                    currentQuality = getQuality(tasks, configuration);
                } else {
                    // The result configuration isn't better, so we go back to the initial configuration
                    this.swapTasks(tasks, configuration, currentTask, lateTask);
                }
            }
        }

        return configuration;
    }

    private int getTimeAtIndex(List<Task> tasks, List<Integer> indices, int index) {
        int currentTime = 0;

        for (int i = 0; i < index; i++) {
            currentTime += tasks.get(indices.get(i)).getLength();
        }

        return currentTime;
    }

    private void swapTasks(List<Task> tasks, List<Integer> indices, Task firstTask, Task secondTask) {
        int firstTaskIndex = indices.indexOf(tasks.indexOf(firstTask));
        int secondTaskIndex = indices.indexOf(tasks.indexOf(secondTask));

        indices.set(firstTaskIndex, tasks.indexOf(secondTask));
        indices.set(secondTaskIndex, tasks.indexOf(firstTask));
    }
}
