package schedulers;

import task.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LatenessConstructiveScheduler implements Scheduler {

    /**
     * A constructive scheduler using the lateness of a task
     */
    public LatenessConstructiveScheduler() {
        super();
    }

    @Override
    public List<Integer> scheduleTasks(List<Task> tasks) {
        ArrayList<Integer> indices = new ArrayList<>();
        for (int i = 0; i < tasks.size(); i++) {
            Task bestTask = getTaskFromLatenessValueAtIndex(tasks, indices);
            indices.add(tasks.indexOf(bestTask));
        }
        return indices;
    }

    /**
     * Get the best task depending on the lateness value at the specified index
     *
     * @param tasks The list of tasks
     * @param indices The list of indices of tasks to exclude
     * @return The best task found
     */
    private Task getTaskFromLatenessValueAtIndex(List<Task> tasks, List<Integer> indices) {
        int currentTime = 0;
        for (Integer index : indices) {
            currentTime += tasks.get(index).getLength();
        }

        int finalCurrentTime = currentTime;
        return tasks.stream()
                .filter(task -> !indices.contains(tasks.indexOf(task)))
                .min((o1, o2) -> o2.getQualityAtTime(finalCurrentTime) - o1.getQualityAtTime(finalCurrentTime))
                .orElseThrow();
    }
}
