package schedulers;

import task.Task;

import java.util.List;

public interface Scheduler {

    /**
     * Return the quality of the scheduled tasks
     *
     * @return The quality of the scheduled tasks
     */
    default int getQuality(List<Task> tasks, List<Integer> indices) {
        int quality = 0;
        int currentTime = 0;
        for (Integer index : indices) {
            currentTime += tasks.get(index).getLength();
            quality += tasks.get(index).getQualityAtTime(currentTime);
        }
        return quality;
    }

    /**
     * From <a href=https://en.wikipedia.org/wiki/Scheduling_(computing)>Wikipedia</a> :
     * <br>
     * Scheduling is the method by which work is assigned to resources that complete the work.
     * The work may be virtual computation elements such as threads, processes or data flows,
     * which are in turn scheduled onto hardware resources such as processors, network links or expansion cards.
     */
    List<Integer> scheduleTasks(List<Task> tasks);
}
